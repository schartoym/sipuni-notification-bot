# Sipuni Notification Bot

Сервис для уведомления о звонках в Sipuni через Telegram.

## Описание

- [Sipuni](https://sipuni.com/) - облачная АТС

Данный сервис принимает уведомления от Sipuni и отправляет их в Telegram.

Описание уведомлений Sipuni: https://sipuni.com/ru_RU/settings/integration/crm_http_api

## Развертывание
Сервис разворачивается в виде фунции в облаке Яндекс.Облако. https://cloud.yandex.ru/services/functions

Для развертывания используется  terraform (https://www.terraform.io/). Описание инфраструктуры находится в папке [terraform](terraform).

Развертывание происходит через Gitlab CI. Он же используется для хранения состояния terraform.

### Переменные
Необходимы переменные для развертывания описаны в файле [variables.tf](terraform/variables.tf)

## Структура проекта
- [Sipuni.Notification.Bot](Sipuni.Notification.Bot) - функция для обработки уведомлений от Sipuni
- [YandexCloudFunctions.Adapter](YandexCloudFunctions.Adapter) - адаптер для обработки вызовов от Яндекс Облака
- [YandexCloudFunctions.Emulator](YandexCloudFunctions.Emulator) - эмулятор для тестирования функции вне облака
- [Sipuni.Notification.Bot.Tests](Sipuni.Notification.Bot.Tests) - тесты для функции, в том числе интеграционные
- [terraform](terraform) - описание инфраструктуры для развертывания функции в облаке
