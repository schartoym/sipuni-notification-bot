using System.Net;
using FluentAssertions;
using Sipuni.Notification.Bot.Models;

namespace Sipuni.Notification.Bot.Tests;

public class EnumHelperTests
{
    [Theory]
    [InlineData("ANSWER", SipuniCallStatus.Answer)]
    [InlineData("BUSY", SipuniCallStatus.Busy)]
    [InlineData("NOANSWER", SipuniCallStatus.NoAnswer)]
    [InlineData("CANCEL", SipuniCallStatus.Cancel)]
    [InlineData("CONGESTION", SipuniCallStatus.Congestion)]
    [InlineData("CHANUNAVAIL", SipuniCallStatus.ChanUnavailable)]
    [InlineData("", null)]
    [InlineData(null, null)]
    [InlineData("SOMETHING_WRONG_", null)]
    public void ParseSipuniCallStatus(string status, SipuniCallStatus? expectedStatus)
    {
        EnumHelper.ToEnum<SipuniCallStatus>(status).Should().Be(expectedStatus);
    }
    
    [Theory]
    [InlineData("1", SipuniEventType.Call)]
    [InlineData("2", SipuniEventType.HangUp)]
    [InlineData("3", SipuniEventType.Answer)]
    [InlineData("4", SipuniEventType.SecondaryHangUp)]
    [InlineData("", null)]
    [InlineData(null, null)]
    [InlineData("     ", null)]
    [InlineData("SOMETHING_WRONG", null)]
    public void ParseSipuniEventType(string eventType, SipuniEventType? expectedEventType)
    {
        EnumHelper.ParseEnumValue<SipuniEventType>(eventType).Should().Be(expectedEventType);
    }
}