using FluentAssertions;
using Microsoft.AspNetCore.WebUtilities;
using Sipuni.Notification.Bot.Models;

namespace Sipuni.Notification.Bot.Tests;

public class SipuniParserTest
{
    /// <summary>
    /// Звонок пришел в систему (на номер компании 84999999999)
    /// </summary>
    [Fact]
    public void CanParseNewCallEventToExternal()
    {
        var queryString =
            "event=1&call_id=1419783130.15593&src_num=89555555555&src_type=1&dst_num=84999999999&dst_type=1&timestamp=1378296000";

        var callEvent = ParseQuery(queryString);

        callEvent.Should().NotBeNull();

        callEvent!.EventType.Should().Be(SipuniEventType.Call);
        callEvent.CallId.Should().Be("1419783130.15593");
        callEvent.SourceNumber.Should().Be("89555555555");
        callEvent.SourceNumberType.Should().Be(SipuniCallType.External);
        callEvent.DestinationNumber.Should().Be("84999999999");
        callEvent.DestinationNumberType.Should().Be(SipuniCallType.External);
        callEvent.CallStatus.Should().BeNull();
        callEvent.CallRecordLink.Should().BeNull();
    }

    private static SipuniCallEvent? ParseQuery(string queryString)
    {
        var query = QueryHelpers.ParseQuery(queryString).ToDictionary(k => k.Key, v => v.Value.ToString());

        return SipuniHelper.ParseEvent(query);
    }

    /// <summary>
    /// Начало звонка с внешнего номера 89555555555 на внутренний номер 101
    /// </summary>
    [Fact]
    public void CanParseNewCallEventToInternal()
    {
        var queryString =
            "event=1&is_inner_call=1&call_id=1419783130.15593&src_num=89555555555&src_type=1&dst_num=012345101&dst_type=2&timestamp=1378296000";

        var callEvent = ParseQuery(queryString);

        callEvent.Should().NotBeNull();

        callEvent!.EventType.Should().Be(SipuniEventType.Call);
        callEvent.CallId.Should().Be("1419783130.15593");
        callEvent.SourceNumber.Should().Be("89555555555");
        callEvent.SourceNumberType.Should().Be(SipuniCallType.External);
        callEvent.DestinationNumber.Should().Be("012345101");
        callEvent.DestinationNumberType.Should().Be(SipuniCallType.Internal);
        callEvent.CallStatus.Should().BeNull();
        callEvent.CallRecordLink.Should().BeNull();
    }

    [Fact]
    public void CanParseAnswerEvent()
    {
        var queryString =
            "event=3&call_id=1419783130.15593&src_num=89555555555&src_type=1&dst_num=012345101&dst_type=2&timestamp=1378296010";

        var callEvent = ParseQuery(queryString);

        callEvent.Should().NotBeNull();

        callEvent!.EventType.Should().Be(SipuniEventType.Answer);
        callEvent.CallId.Should().Be("1419783130.15593");
        callEvent.SourceNumber.Should().Be("89555555555");
        callEvent.SourceNumberType.Should().Be(SipuniCallType.External);
        callEvent.DestinationNumber.Should().Be("012345101");
        callEvent.DestinationNumberType.Should().Be(SipuniCallType.Internal);
        callEvent.CallStatus.Should().BeNull();
        callEvent.CallRecordLink.Should().BeNull();
    }

    [Fact]
    public void CanParseHangUpEvent()
    {
        var queryString =
            "event=2&call_id=1419783130.15593&src_num=89555555555&src_type=1&dst_num=012345102&dst_type=2&call_start_timestamp=1378296000&call_answer_timestamp=1378296025&timestamp=1378296050&status=ANSWER&call_record_link=ftp%3a%2f%2fmyhost.info%2faccount%2frecords%2f354435435436.wav";

        var callEvent = ParseQuery(queryString);

        callEvent.Should().NotBeNull();

        callEvent!.EventType.Should().Be(SipuniEventType.HangUp);
        callEvent.CallId.Should().Be("1419783130.15593");
        callEvent.SourceNumber.Should().Be("89555555555");
        callEvent.SourceNumberType.Should().Be(SipuniCallType.External);
        callEvent.DestinationNumber.Should().Be("012345102");
        callEvent.DestinationNumberType.Should().Be(SipuniCallType.Internal);
        callEvent.CallStatus.Should().Be(SipuniCallStatus.Answer);
        callEvent.CallRecordLink.Should().Be("ftp://myhost.info/account/records/354435435436.wav");
    }
}