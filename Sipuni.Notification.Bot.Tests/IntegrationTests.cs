using System.Net;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc.Testing;

namespace Sipuni.Notification.Bot.Tests;

/// <summary>
/// Интеграционные тесты. Необходимо предварительно прописать в appsettings.json токен бота и id чата
/// </summary>
[Trait("Category", "Integration")]
public class IntegrationTests : IClassFixture<WebApplicationFactory<Program>>
{
    private readonly WebApplicationFactory<Program> _factory;

    public IntegrationTests(WebApplicationFactory<Program> fixture)
    {
        _factory = fixture;
    }

    [Theory]
    [InlineData("event=1&call_id=1419783130.15593&src_num=89555555555&src_type=1&dst_num=84999999999&dst_type=1&timestamp=1378296000")]
    [InlineData("event=1&is_inner_call=1&call_id=1419783130.15593&src_num=89555555555&src_type=1&dst_num=012345101&dst_type=2&timestamp=1378296000")]
    [InlineData("event=3&call_id=1419783130.15593&src_num=89555555555&src_type=1&dst_num=012345101&dst_type=2&timestamp=1378296010")]
    [InlineData("event=2&call_id=1419783130.15593&src_num=89555555555&src_type=1&dst_num=012345102&dst_type=2&call_start_timestamp=1378296000&call_answer_timestamp=1378296025&timestamp=1378296050&status=ANSWER&call_record_link=http%3a%2f%2fmyhost.info%2faccount%2frecords%2f354435435436.wav")]
    public async Task CanHandleRequest(string queryString)
    {
        var client = _factory.CreateClient();
        var requestUri = new Uri($"{client.BaseAddress}?{queryString}");
        var response = await client.PostAsync(requestUri, new StringContent(""));
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
}