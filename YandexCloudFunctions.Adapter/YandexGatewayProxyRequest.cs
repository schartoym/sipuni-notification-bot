namespace YandexCloudFunctions.Adapter;

public class YandexGatewayProxyRequest
{
    /// <summary>
    /// ресурс, соответствующий запросу в спецификации
    /// </summary>
    public string Resource { get; set; } = "";

    /// <summary>
    /// фактический путь запроса
    /// </summary>
    public string Path { get; set; } = "";

    /// <summary>
    /// название HTTP-метода
    /// </summary>
    public string HttpMethod { get; set; } = "";

    /// <summary>
    /// словарь со строковыми значениями HTTP-заголовков
    /// </summary>
    public IDictionary<string, string> Headers { get; set; } = new Dictionary<string, string>();

    /// <summary>
    /// словарь со списками значений HTTP-заголовков
    /// </summary>
    public IDictionary<string, List<string?>> MultiValueHeaders { get; set; } = new Dictionary<string, List<string?>>();

    /// <summary>
    /// словарь queryString-параметров
    /// </summary>
    public IDictionary<string, string> QueryStringParameters { get; set; } = new Dictionary<string, string>();

    /// <summary>
    /// словарь списков значений queryString-параметров
    /// </summary>
    public IDictionary<string, List<string?>> MultiValueQueryStringParameters { get; set; } = new Dictionary<string, List<string?>>();

    /// <summary>
    /// словарь значений параметров пути запроса
    /// </summary>
    public IDictionary<string, string> PathParameters { get; set; } = new Dictionary<string, string>();

    public YandexProxyRequestContext RequestContext { get; set; } = new();

    /// <summary>
    /// содержимое запроса
    /// </summary>
    public string Body { get; set; } = "";

    public bool IsBase64Encoded { get; set; }

    /// <summary>
    /// словарь значений параметров запроса, описанных в спецификации OpenAPI
    /// </summary>
    public IDictionary<string, string> Parameters = new Dictionary<string, string>();
    
    /// <summary>
    /// словарь со списками значений параметров запроса, описанных в спецификации OpenAPI
    /// </summary>
    public IDictionary<string, IList<string>> MultiValueParameters = new Dictionary<string, IList<string>>();

    /// <summary>
    /// operationId, соответствующий запросу в спецификации OpenAPI
    /// </summary>
    public string OperationId { get; set; } = "";
}