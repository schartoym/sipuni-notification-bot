using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

namespace YandexCloudFunctions.Adapter.Models;

[Browsable(false)]
[SuppressMessage("ReSharper", "InconsistentNaming")]
public class YandexProxyRequestContextModel
{
    /// <summary>
    /// идентификатор запроса, который генерируется в роутере
    /// </summary>
    public string requestId { get; set; } = "";

    /// <summary>
    /// набор пар ключ:значение для аутентификации пользователя
    /// </summary>
    public IDictionary<string, string> identity { get; set; } = new Dictionary<string, string>();

    /// <summary>
    /// название HTTP-метода
    /// </summary>
    public string httpMethod { get; set; } = "";

    /// <summary>
    /// идентификатор веб-сокетного соединения
    /// </summary>
    public string connectionId { get; set; } = "";

    /// <summary>
    /// время подключения веб-сокетного соединения
    /// </summary>
    public long connectedAt { get; set; }

    /// <summary>
    /// тип события или операции с веб-сокетом: CONNECT, MESSAGE, DISCONNECT
    /// </summary>
    public string eventType { get; set; } = "";

    /// <summary>
    /// идентификатор сообщения, полученного из веб-сокета
    /// </summary>
    public string messageId { get; set; } = "";
    
    /// <summary>
    /// время запроса в формате CLF
    /// </summary>
    public string requestTime { get; set; } = "";
    
    /// <summary>
    /// время запроса в формате Unix
    /// </summary>
    public long requestTimeEpoch { get; set; }

    /// <summary>
    /// статус-код закрытия веб-сокета
    /// </summary>
    public int disconnectStatusCode { get; set; }

    /// <summary>
    /// текстовое описание причины закрытия веб-сокета
    /// </summary>
    public string disconnectReason { get; set; } = "";
}