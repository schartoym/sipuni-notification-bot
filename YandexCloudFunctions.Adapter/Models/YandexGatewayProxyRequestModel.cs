using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

namespace YandexCloudFunctions.Adapter.Models;

[Browsable(false)]
[SuppressMessage("ReSharper", "InconsistentNaming")]
public class YandexGatewayProxyRequestModel
{
    /// <summary>
    /// ресурс, соответствующий запросу в спецификации
    /// </summary>
    public string resource { get; set; } = "";

    /// <summary>
    /// фактический путь запроса
    /// </summary>
    public string path { get; set; } = "";

    /// <summary>
    /// название HTTP-метода
    /// </summary>
    public string httpMethod { get; set; } = "";

    /// <summary>
    /// словарь со строковыми значениями HTTP-заголовков
    /// </summary>
    public IDictionary<string, string> headers { get; set; } = new Dictionary<string, string>();

    /// <summary>
    /// словарь со списками значений HTTP-заголовков
    /// </summary>
    public Dictionary<string, List<string?>> multiValueHeaders { get; set; } = new();

    /// <summary>
    /// словарь queryString-параметров
    /// </summary>
    public IDictionary<string, string> queryStringParameters { get; set; } = new Dictionary<string, string>();

    /// <summary>
    /// словарь списков значений queryString-параметров
    /// </summary>
    public Dictionary<string, List<string?>> multiValueQueryStringParameters { get; set; } = new();

    /// <summary>
    /// словарь значений параметров пути запроса
    /// </summary>
    public IDictionary<string, string> pathParameters { get; set; } = new Dictionary<string, string>();

    public YandexProxyRequestContextModel requestContext { get; set; } = new();

    /// <summary>
    /// содержимое запроса
    /// </summary>
    public string body { get; set; } = "";

    public bool isBase64Encoded { get; set; }

    /// <summary>
    /// словарь значений параметров запроса, описанных в спецификации OpenAPI
    /// </summary>
    public IDictionary<string, string> parameters = new Dictionary<string, string>();
    
    /// <summary>
    /// словарь со списками значений параметров запроса, описанных в спецификации OpenAPI
    /// </summary>
    public IDictionary<string, IList<string>> multiValueParameters = new Dictionary<string, IList<string>>();

    /// <summary>
    /// operationId, соответствующий запросу в спецификации OpenAPI
    /// </summary>
    public string operationId { get; set; } = "";
}

