using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace YandexCloudFunctions.Adapter;

public static class JsonHelper
{
    public static JsonSerializerSettings JsonSerializerSettings { get; } = new()
    {
        NullValueHandling = NullValueHandling.Ignore,
        Converters = new List<JsonConverter>
        {
            new StringEnumConverter()
        }
    };
    
    public static string Serialize<T>(T obj)
    {
        return JsonConvert.SerializeObject(obj, JsonSerializerSettings);
    }
}