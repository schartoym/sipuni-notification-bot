using System.Net;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace YandexCloudFunctions.Adapter;

[DataContract]
public class YandexGatewayProxyResponse
{
    public YandexGatewayProxyResponse(HttpStatusCode statusCode, string? body = null)
    {
        StatusCode = (int)statusCode;
        Body = body;
    }

    [DataMember(Name = "statusCode")]
    public int StatusCode { get; set; }

    [DataMember(Name = "headers")]
    public IDictionary<string, string> Headers { get; set; } = new Dictionary<string, string>();

    [DataMember(Name = "multiValueHeaders")]
    public IDictionary<string, IList<string>> MultiValueHeaders { get; set; } = new Dictionary<string, IList<string>>();

    [DataMember(Name = "body")]
    public string? Body { get; set; }

    [DataMember(Name = "isBase64Encoded")]
    public bool IsBase64Encoded { get; set; }
    
    public static YandexGatewayProxyResponse BadRequest<T>(T? result = null) where T : class
    {
        return new YandexGatewayProxyResponse(HttpStatusCode.BadRequest, JsonHelper.Serialize(result));
    }
    
    public static YandexGatewayProxyResponse Ok<T>(T? result = null) where T : class
    {
        return new YandexGatewayProxyResponse(HttpStatusCode.OK, JsonHelper.Serialize(result));
    }
}