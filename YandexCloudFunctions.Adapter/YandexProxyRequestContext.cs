namespace YandexCloudFunctions.Adapter;

public class YandexProxyRequestContext
{
    /// <summary>
    /// идентификатор запроса, который генерируется в роутере
    /// </summary>
    public string RequestId { get; set; } = "";

    /// <summary>
    /// набор пар ключ:значение для аутентификации пользователя
    /// </summary>
    public IDictionary<string, string> Identity { get; set; } = new Dictionary<string, string>();

    /// <summary>
    /// название HTTP-метода
    /// </summary>
    public string HttpMethod { get; set; } = "";

    /// <summary>
    /// идентификатор веб-сокетного соединения
    /// </summary>
    public string ConnectionId { get; set; } = "";

    /// <summary>
    /// время подключения веб-сокетного соединения
    /// </summary>
    public long ConnectedAt { get; set; }

    /// <summary>
    /// тип события или операции с веб-сокетом: CONNECT, MESSAGE, DISCONNECT
    /// </summary>
    public string EventType { get; set; } = "";

    /// <summary>
    /// идентификатор сообщения, полученного из веб-сокета
    /// </summary>
    public string MessageId { get; set; } = "";
    
    /// <summary>
    /// время запроса в формате CLF
    /// </summary>
    public string RequestTime { get; set; } = "";
    
    /// <summary>
    /// время запроса в формате Unix
    /// </summary>
    public long RequestTimeEpoch { get; set; }

    /// <summary>
    /// статус-код закрытия веб-сокета
    /// </summary>
    public int DisconnectStatusCode { get; set; }

    /// <summary>
    /// текстовое описание причины закрытия веб-сокета
    /// </summary>
    public string DisconnectReason { get; set; } = "";
}