﻿using JetBrains.Annotations;
using Serilog;
using YandexCloudFunctions.Adapter.Models;

namespace YandexCloudFunctions.Adapter;

public abstract class ApiGatewayYandexFunction
{
    protected readonly ILogger Logger = new LoggerConfiguration()
        .Enrich
        .FromLogContext()
        .WriteTo.Console()
        .WriteTo.Sentry()
        .CreateLogger()
        .ForContext<ApiGatewayYandexFunction>();

    protected abstract Task<YandexGatewayProxyResponse> HandleRequest(YandexGatewayProxyRequest request);

    [UsedImplicitly]
    public async Task<string> FunctionHandler(YandexGatewayProxyRequestModel requestModel)
    {
        Logger.Information("Handling request {OperationId}", requestModel.operationId);
        
        var ctx = requestModel.requestContext;
        var request = new YandexGatewayProxyRequest()
        {
            Body = requestModel.body,
            Headers = requestModel.headers,
            Path = requestModel.path,
            HttpMethod = requestModel.httpMethod,
            Resource = requestModel.resource,
            PathParameters = requestModel.pathParameters,
            RequestContext = new YandexProxyRequestContext
            {
                HttpMethod = ctx.httpMethod,
                RequestId = ctx.requestId,
                RequestTime = ctx.requestTime,
                RequestTimeEpoch = ctx.requestTimeEpoch,
                ConnectionId = ctx.connectionId,
                ConnectedAt = ctx.connectedAt,
                EventType = ctx.eventType,
                MessageId = ctx.messageId,
                DisconnectReason  = ctx.disconnectReason,
                DisconnectStatusCode = ctx.disconnectStatusCode,
                Identity = ctx.identity
            },
            IsBase64Encoded = requestModel.isBase64Encoded,
            Parameters = requestModel.parameters,
            OperationId = requestModel.operationId,
            MultiValueHeaders = requestModel.multiValueHeaders,
            QueryStringParameters = requestModel.queryStringParameters,
            MultiValueParameters = requestModel.multiValueParameters,
            MultiValueQueryStringParameters = requestModel.multiValueQueryStringParameters,
        };

        YandexGatewayProxyResponse result;
        
        try
        {
            result = await HandleRequest(request);
        }
        catch (Exception e)
        {
            Logger.Error(e, "Unable to handle request {OperationId}", requestModel.operationId);
            throw;
        }

        result.Headers["Content-Type"] = "application/json";

        return JsonHelper.Serialize(result);
    }
}