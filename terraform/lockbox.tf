resource "yandex_lockbox_secret" "function_secret" {
  name = "sipuini-notification-bot-secret"
}

resource "yandex_lockbox_secret_version" "function_secret_version" {
  secret_id = yandex_lockbox_secret.function_secret.id
  entries {
    key        = "telegram_token"
    text_value = var.telegram_token
  }
  entries {
    key        = "telegram_chat_id"
    text_value = var.telegram_chat_id
  }
}
