locals {
  filepath = "../artifacts/code.zip"
}

data "local_file" "function_code" {
  filename = local.filepath
}

resource "yandex_function" "function" {
  name               = var.function_name
  description        = "Send Sipuni Call notifications to Telegram"
  user_hash          = data.local_file.function_code.content_sha256
  runtime            = "dotnet6"
  entrypoint         = "Sipuni.Notification.Bot.SipuniWebhookHandler"
  memory             = "256"
  execution_timeout  = "10"
  service_account_id = var.service_account_id
  content {
    zip_filename = data.local_file.function_code.filename
  }
  secrets {
    id                   = yandex_lockbox_secret.function_secret.id
    version_id           = yandex_lockbox_secret_version.function_secret_version.id
    key                  = "telegram_token"
    environment_variable = "Telegram__Token"
  }
  secrets {
    id                   = yandex_lockbox_secret.function_secret.id
    version_id           = yandex_lockbox_secret_version.function_secret_version.id
    key                  = "telegram_chat_id"
    environment_variable = "Telegram__ChatId"
  }

}

resource "yandex_function_iam_binding" "function-iam" {
  function_id = yandex_function.function.id
  role        = "functions.functionInvoker"
  members = [
    "system:allUsers",
  ]
}


output "function_id" {
  value = yandex_function.function.id
}
