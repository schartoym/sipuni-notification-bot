variable "cloud_token" {
  type        = string
  description = "Yandex Cloud token"
  sensitive   = true
}

variable "cloud_id" {
  type        = string
  description = "Yandex Cloud Id"
}

variable "folder_id" {
  type        = string
  description = "Yandex Cloud Folder Id"
}

variable "service_account_id" {
  type        = string
  description = "Yandex Service Account Id"
}

variable "zone" {
  type        = string
  default     = "ru-central1-a"
  description = "Yandex Cloud Zone"
}

variable "function_name" {
  type    = string
  default = "send-sipuni-notifications-to-telegram"
}

variable "telegram_token" {
  type        = string
  description = "Telegram Bot Token"
  sensitive   = true
}

variable "telegram_chat_id" {
  type        = string
  description = "Telegram Chat Id where to send notifications"
  sensitive   = true
}