using Newtonsoft.Json;
using Sipuni.Notification.Bot;
using YandexCloudFunctions.Adapter;
using YandexCloudFunctions.Emulator;

var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();

app.Map("/", async (HttpRequest request) =>
{
    var yandexGatewayRequest = await request.GetYandexGatewayProxyRequestModel();
    var handler = new SipuniWebhookHandler();

    var response = await handler.FunctionHandler(yandexGatewayRequest);

    var responseObject = JsonConvert
        .DeserializeObject<YandexGatewayProxyResponse>(response, JsonHelper.JsonSerializerSettings);

    return new YandexGatewayProxyResponseResult(responseObject!);
});

app.Run();

public partial class Program {}