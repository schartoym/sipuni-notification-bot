using YandexCloudFunctions.Adapter.Models;

namespace YandexCloudFunctions.Emulator;

public static class HttpRequestHelper
{
    public static async Task<string> GetBodyString(this HttpRequest request)
    {
        request.EnableBuffering();
        using var streamReader = new StreamReader(request.Body, leaveOpen: true);
        request.Body.Position = 0;
        var requestBody = await streamReader.ReadToEndAsync();
        request.Body.Position = 0;
        return requestBody;
    }

    public static async Task<YandexGatewayProxyRequestModel> GetYandexGatewayProxyRequestModel(this HttpRequest request)
    {
        var body = await request.GetBodyString();

        return new YandexGatewayProxyRequestModel
        {
            body = body,
            isBase64Encoded = false,
            headers = request.Headers.ToDictionary(h => h.Key,
                h => h.Value.ToString()),
            multiValueHeaders = request.Headers
                .ToDictionary(h => h.Key, h => h.Value.ToList()),

            queryStringParameters = request.Query
                .ToDictionary(q => q.Key, q => q.Value.ToString()),
            multiValueQueryStringParameters = request.Query
                .ToDictionary(q => q.Key, q => q.Value.ToList()),
            requestContext = new YandexProxyRequestContextModel()
            {
                httpMethod = request.Method,
                connectionId = request.HttpContext.Connection.Id,
                requestId = Guid.NewGuid().ToString(),
                requestTime = DateTime.UtcNow.ToString("O"),
            },
            path = request.Path,
            httpMethod = request.Method,
            operationId = $"{request.Method}_{request.Path}",
        };
    }
}