using YandexCloudFunctions.Adapter;

namespace YandexCloudFunctions.Emulator;

public class YandexGatewayProxyResponseResult : IResult
{
    private readonly YandexGatewayProxyResponse _response;

    public YandexGatewayProxyResponseResult(YandexGatewayProxyResponse response)
    {
        _response = response;
    }
    public async Task ExecuteAsync(HttpContext httpContext)
    {
        httpContext.Response.StatusCode = _response.StatusCode;

        foreach (var header in _response.Headers)
        {
            httpContext.Response.Headers.Add(header.Key, header.Value);
        }

        if (!string.IsNullOrWhiteSpace(_response.Body))
        {
            await httpContext.Response.WriteAsync(_response.Body);
        }
    }
}