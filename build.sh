#!/bin/sh

set -e

PROJECT=Sipuni.Notification.Bot/Sipuni.Notification.Bot.csproj
OUTPUT_DIR=build
OUTPUT_ZIP=code.zip

rm -rf ./artifacts/$OUTPUT_DIR
rm -rf ./artifacts/$OUTPUT_ZIP

dotnet publish $PROJECT -c Production \
 --runtime linux-x64 \
 --framework net6.0 \
 --no-self-contained \
 --output artifacts/$OUTPUT_DIR \
 --packages artifacts/nuget-packages

cd ./artifacts/$OUTPUT_DIR
zip -r $OUTPUT_ZIP .
mv $OUTPUT_ZIP ../
