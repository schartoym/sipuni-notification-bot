using System.Runtime.Serialization;

namespace Sipuni.Notification.Bot;

public static class EnumHelper
{
    public static T? ToEnum<T>(string str) where T: struct
    {
        var enumType = typeof(T);

        foreach (var name in Enum.GetNames(enumType))
        {
            var fieldInfo = enumType.GetField(name);

            if (fieldInfo == null)
            {
                continue;
            }
            
            var enumMemberAttribute = ((EnumMemberAttribute[])fieldInfo
                    .GetCustomAttributes(typeof(EnumMemberAttribute), true))
                .Single();
            
            if (enumMemberAttribute.Value == str)
            {
                return (T)Enum.Parse(enumType, name);
            }
        }
        
        return null;
    }
    
    public static T? ParseEnumValue<T>(string? str) where T: struct
    {
        if (int.TryParse(str, out var intResult))
        {
            return (T)Enum.ToObject(typeof(T), intResult);
        }

        return Enum.TryParse(typeof(T), str, true, out var result)
            ? (T?)result
            : null;
    }
}