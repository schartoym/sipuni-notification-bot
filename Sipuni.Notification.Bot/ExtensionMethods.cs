namespace Sipuni.Notification.Bot;

public static class ExtensionMethods
{
    /// <summary>
    /// Обрезает текст до требуемого количества символов, если количество символов в строке больше заданного лимита
    /// Если Truncate отработал, то вконец добавляется многоточие
    /// </summary>
    public static string Truncate(this string text, int limit)
    {
        if (string.IsNullOrEmpty(text) || limit >= text.Length)
        {
            return text;
        }

        return text[..(limit-3)] + "...";
    }

    public static string EscapeForTelegram(this string text)
    {
        if (string.IsNullOrWhiteSpace(text))
        {
            return text;
        }
        
        return text
                .Replace("_", "\\_")
                .Replace("-", "\\-")
                .Replace("+", "\\+")
                .Replace("*", "\\*")
                .Replace("[", "\\[")
                .Replace("]", "\\]")
                .Replace("`", "\\`")
                .Replace("(", "\\(")
                .Replace(")", "\\)")
                .Replace("|", "\\|")
                .Replace(".", "\\.")
                .Replace("#", "\\#")
            ;
    }

    public static string ToHashTag(this string text, string prefix)
    {
        return $"#{prefix}{text.Replace(".", "_")}".EscapeForTelegram();
    }
}