namespace Sipuni.Notification.Bot;

public class TelegramSettings
{
    public string Token { get; set; } = "";
    public long ChatId { get; set; }
}