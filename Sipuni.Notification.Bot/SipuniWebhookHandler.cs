﻿using JetBrains.Annotations;
using Microsoft.Extensions.Configuration;
using Sipuni.Notification.Bot.Models;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using YandexCloudFunctions.Adapter;

namespace Sipuni.Notification.Bot;

[UsedImplicitly]
public class SipuniWebhookHandler : ApiGatewayYandexFunction
{
    private readonly TelegramBotClient _bot;

    private readonly TelegramSettings _settings;
    
    public SipuniWebhookHandler()
    {
        var configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .AddJsonFile("appsettings.overrides.json", true)
            .AddEnvironmentVariables()
            .Build();

        var settings = configuration.GetRequiredSection("Telegram").Get<TelegramSettings>();

        _settings = settings ?? throw new ArgumentException("Settings is null");
        
        if (string.IsNullOrWhiteSpace(settings.Token))
        {
            Logger.Error("Telegram token is not set");
            throw new ArgumentException("Telegram token is not set");
        }
        
        _bot = new TelegramBotClient(_settings.Token);
    }

    protected override async Task<YandexGatewayProxyResponse> HandleRequest(YandexGatewayProxyRequest request)
    {
        var sipuniEvent = SipuniHelper.ParseEvent(request.QueryStringParameters);
        
        if(sipuniEvent == null)
        {
            Logger.Error("Unable to parse Sipuni event. {QueryString}", request.QueryStringParameters);
            
            return YandexGatewayProxyResponse.BadRequest(new SipuniResponse(false));
        }

        Logger.Information("Handling Sipuni event {EventType}: {@Event}", sipuniEvent.EventType, sipuniEvent);
        
        var action = sipuniEvent.EventType switch
        {
            SipuniEventType.Call => HandleCallEvent(sipuniEvent),
            SipuniEventType.HangUp => HandleHangUpEvent(sipuniEvent),
            SipuniEventType.Answer => HandleAnswerEvent(sipuniEvent),
            SipuniEventType.SecondaryHangUp => HandleHangUpEvent(sipuniEvent),
            _ => throw new ArgumentOutOfRangeException()
        };

        await action;
        
        return YandexGatewayProxyResponse.Ok(new SipuniResponse(true));
    }

    private Task HandleAnswerEvent(SipuniCallEvent callEvent)
    {
        var message = @$"📞 *Звонок принят*
{GetCallDescription(callEvent)}";

        return SendToTelegram(message);
    }

    private Task HandleHangUpEvent(SipuniCallEvent callEvent)
    {
        var message = @$"🤙 *Звонок завершен*

{GetCallDescription(callEvent)}

*Статус звонка*: `{callEvent.CallStatus.GetValueOrDefault().GetDescription()}`
";
        var buttons = new List<InlineKeyboardButton>();
        
        if (!string.IsNullOrWhiteSpace(callEvent.CallRecordLink) && callEvent.CallRecordLink.StartsWith("http"))
        {
            buttons.Add(InlineKeyboardButton.WithUrl("Запись разговора", callEvent.CallRecordLink));
        }

        return SendToTelegram(message, new InlineKeyboardMarkup(buttons));
    }

    private  Task HandleCallEvent(SipuniCallEvent callEvent)
    {
        var message = @$"🔔 *Новый звонок*
{GetCallDescription(callEvent)}";

        return SendToTelegram(message);
    }

    private static string GetCallDescription(SipuniCallEvent callEvent)
    {
        var sourceNumber = callEvent.SourceNumberType switch
        {
            SipuniCallType.External => $"+{callEvent.SourceNumber}",
            _ => callEvent.ShortSourceNumber,
        };
        
        var destinationNumber = callEvent.DestinationNumberType switch
        {
            SipuniCallType.External => $"{callEvent.DestinationNumber}",
            _ => callEvent.ShortDestinationNumber,
        };
        
        return @$"
*С номера*: {sourceNumber.EscapeForTelegram()}
*На номер*: `{destinationNumber?.EscapeForTelegram()}`
{callEvent.CallId.ToHashTag("call_")}";
        
    }

    private async Task SendToTelegram(string message, IReplyMarkup? replyMarkup = null)
    {
        await _bot.SendTextMessageAsync(_settings.ChatId,
            message,
            replyMarkup: replyMarkup,
            parseMode: ParseMode.MarkdownV2);
    }
}