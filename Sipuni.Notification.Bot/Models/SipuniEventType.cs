namespace Sipuni.Notification.Bot.Models;

public enum SipuniEventType
{
    /// <summary>
    /// событие начала звонка event
    /// </summary>
    Call = 1,
    
    /// <summary>
    /// событие окончания звонка event
    /// </summary>
    HangUp = 2,
    
    /// <summary>
    /// ответ на вызов
    /// </summary>
    Answer = 3,
    
    /// <summary>
    /// промежуточное завершение вызова 
    /// </summary>
    SecondaryHangUp = 4,
}