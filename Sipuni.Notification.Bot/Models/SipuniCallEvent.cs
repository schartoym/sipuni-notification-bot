namespace Sipuni.Notification.Bot.Models;

/// <summary>
/// Модель данных присылаемых Sipuni
/// https://help.sipuni.com/articles/134-182-110--otpravka-sobytij-ats-na-skript-http-api/
/// </summary>
public class SipuniCallEvent
{
    /// <summary>
    /// event - тип запроса
    /// </summary>
    public SipuniEventType EventType { get; set; }
    
    /// <summary>
    /// call_id  - уникальный идентификатор вызова (сохраняется неизменным при переводе),
    /// является строкой произвольного формата (с использованием URL кодировки)
    /// </summary>
    public string CallId { get; set; } = "";

    /// <summary>
    /// src_num адрес абонента инициализировавшего вызов (сохраняется при переводе) 
    /// </summary>
    public string SourceNumber { get; set; } = "";
    
    public SipuniCallType SourceNumberType { get; set; }
    
    /// <summary>
    /// dst_num адрес назначения – может быть пустым при запросе на «умную переадресацию
    /// </summary>
    public string? DestinationNumber { get; set; }
    
    public SipuniCallType? DestinationNumberType { get; set; }
    
    public DateTimeOffset Timestamp { get; set; }

    /// <summary>
    /// внутренний номер инициатора звонка.
    /// Пример - вызов поступает с мобильного номера зафиксированного за одним из наших сотрудников в конструкторе
    /// или идет исходящий звонок с SIP-устройства сотрудника.
    /// </summary>
    public string ShortSourceNumber { get; set; } = "";
    
    /// <summary>
    /// внутренний номер назначения.
    /// </summary>
    public string ShortDestinationNumber { get; set; } = "";
    
    /// <summary>
    /// Статус звонка
    /// </summary>
    public SipuniCallStatus? CallStatus { get; set; }
    
    /// <summary>
    /// Времня начала вызова
    /// </summary>
    public DateTimeOffset? CallStartTime { get; set; }
    
    /// <summary>
    /// Время ответа на вызов, в случае отсутствия ответа, значение данного параметра должно быть равно 0.
    /// </summary>
    public DateTimeOffset? CallAnswerTime { get; set; }
    
    /// <summary>
    /// URL на файл записи разговора (в URL кодировке).
    /// </summary>
    public string? CallRecordLink { get; set; }
}