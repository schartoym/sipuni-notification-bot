using System.Net;

namespace Sipuni.Notification.Bot.Models;

public static class SipuniHelper
{
    public static DateTimeOffset? TryParseTimestamp(string? timestampString)
    {
        if (!long.TryParse(timestampString, out var timestamp))
        {
            return null;
        }
        
        if (timestamp == 0)
        {
            return null;
        }
            
        return DateTimeOffset.FromUnixTimeSeconds(timestamp);
    }

    public static string? TryDecode(string? recordLink)
    {
        if (string.IsNullOrWhiteSpace(recordLink))
        {
            return null;
        }

        return WebUtility.UrlDecode(recordLink);
    }
    
    public static SipuniCallEvent? ParseEvent(IDictionary<string, string> query)
    {
        if (!query.TryGetValue("event", out var eventTypeStr))
        {
            return null;
        }
        
        var eventType = EnumHelper.ParseEnumValue<SipuniEventType>(eventTypeStr);

        if (!eventType.HasValue)
        {
            return null;
        }

        var evt = new SipuniCallEvent
        {
            EventType = eventType.Value,
            CallId = query.GetValueOrDefault("call_id") ?? "unknown",
            SourceNumber = query.GetValueOrDefault("src_num") ?? "unknown",
            SourceNumberType = EnumHelper.ParseEnumValue<SipuniCallType>(query.GetValueOrDefault("src_type")) ?? SipuniCallType.Unknown,
            DestinationNumber = query.GetValueOrDefault("dst_num"),
            DestinationNumberType = EnumHelper.ParseEnumValue<SipuniCallType>(query.GetValueOrDefault("dst_type")) ?? SipuniCallType.Unknown,
            Timestamp = TryParseTimestamp(query.GetValueOrDefault("timestamp")) ?? DateTimeOffset.UtcNow,
            ShortSourceNumber = query.GetValueOrDefault("short_src_num") ?? "unknown",
            ShortDestinationNumber = query.GetValueOrDefault("short_dst_num") ?? "unknown",
            CallStatus = EnumHelper.ParseEnumValue<SipuniCallStatus>(query.GetValueOrDefault("status")),
            CallStartTime = TryParseTimestamp(query.GetValueOrDefault("call_start_timestamp")),
            CallAnswerTime = TryParseTimestamp(query.GetValueOrDefault("call_answer_timestamp")),
            CallRecordLink = TryDecode(query.GetValueOrDefault("call_record_link"))
        };

        return evt;
    }
    
    public static TValue? GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue? defaultValue = default)
    {
        return dictionary.TryGetValue(key, out var value) ? value : defaultValue;
    }

    public static string GetDescription(this SipuniCallStatus callStatus)
    {
        return callStatus switch
        {
            SipuniCallStatus.Answer => "Отвечен",
            SipuniCallStatus.Busy => "Занято",
            SipuniCallStatus.NoAnswer => "Не отвечен",
            SipuniCallStatus.Cancel => "Отменен",
            SipuniCallStatus.Congestion => "Перегрузка сети",
            SipuniCallStatus.ChanUnavailable => "Абонент недоступен",
            _ => callStatus.ToString()
        };
    }
}