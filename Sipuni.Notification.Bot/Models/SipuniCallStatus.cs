using System.Runtime.Serialization;

namespace Sipuni.Notification.Bot.Models;

/// <summary>
/// Возможные статусы звонка
/// </summary>
public enum SipuniCallStatus
{
    /// <summary>
    /// вызов отвечен 
    /// </summary>
    [EnumMember(Value = "ANSWER")]
    Answer,
    
    /// <summary>
    /// абонент занят
    /// </summary>
    [EnumMember(Value = "BUSY")]
    Busy,
    
    /// <summary>
    /// абонент не ответил после определённого таймаута 
    /// </summary>
    [EnumMember(Value = "NOANSWER")]
    NoAnswer,
    
    /// <summary>
    /// вызов сброшен
    /// </summary>
    [EnumMember(Value = "CANCEL")]
    Cancel,
    
    /// <summary>
    /// перегрузка сети
    /// </summary>
    [EnumMember(Value = "CONGESTION")]
    Congestion,
    
    /// <summary>
    /// абонент недоступен (например sip абонент не зарегистрирован в сети)
    /// </summary>
    [EnumMember(Value = "CHANUNAVAIL")]
    ChanUnavailable
}