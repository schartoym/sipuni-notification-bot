using Newtonsoft.Json;

namespace Sipuni.Notification.Bot.Models;

public class SipuniResponse
{
    [JsonProperty("success")]
    public bool Success { get; }

    public SipuniResponse(bool isSuccess)
    {
        Success = isSuccess;
    }
}