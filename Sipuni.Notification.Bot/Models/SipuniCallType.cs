namespace Sipuni.Notification.Bot.Models;

/// <summary>
/// тип адреса 
/// </summary>
public enum SipuniCallType
{
    External = 1,
    
    Internal = 2,
    
    Unknown = -1,
}